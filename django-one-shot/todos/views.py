from django.shortcuts import render, get_object_or_404
from .models import TodoList
from .models import TodoItem

# Create your views here.
def TodoList(request, id):
    list = get_object_or_404(list, id=id)
    context = {
        "TodoList, on_delete=models, related_name='items'"
    }

def todo_item_view(request):
    items = TodoItem.objects.all()
    return ('Here are your items: %s' % items)