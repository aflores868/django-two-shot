from django.urls import path
from .views import receipt_list_view

urlpatterns = [
    path('', receipt_list_view, name='home'),
]