from django.shortcuts import render
from django.views.generic import ListView
from .models import Receipt
from django.contrib.auth.decorators import login_required

# Create your views here.
@login_required
def receipt_list_view(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    return render(request, 'receipts/create.html', {'receipts': receipts})