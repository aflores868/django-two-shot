from django.urls import path, include
from django.contrib.auth import views
from .views import login_view
from .views import logout_view
from .views import signup_view
from .views import create_receipt

urlpatterns = [
    path('login/', login_view, name='login'),
    path('logout/', logout_view, name='logout'),
    path('signup/', signup_view, name='signup'),
    path('create/', create_receipt, name='create_receipt'),
    path('accounts/', include(('django.contrib.auth.urls', 'registration'))),
]