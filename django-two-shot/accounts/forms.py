from django import forms
from django.contrib.auth.models import User
from .models import Receipt

class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput())

class SignUpForm(forms.ModelForm):
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)
    password_configuration = forms.CharField(max_length=150, widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ['username', 'password', 'password_configuration']

class ReceiptForm(forms.ModelForm):
    class Meta:
        model = Receipt
        fields = ['vendor', 'total', 'tax', 'date', 'category', 'account']